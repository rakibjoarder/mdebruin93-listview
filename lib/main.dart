import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/blocprovider/main_bloc_provider.dart';
import 'package:flutter_app/screen/home_page.dart';

void main() => runApp(MyApp());
//Please check all the code comment
//we have two separate bloc,
//- chat_bloc = chat_screen's business logic is here
//- main_bloc = home_page's business logic is here
//blocprovider is for holding the bloc state.
//please after successfully login save your senderId , username in sharedpreference

// in lib/bloc/chat_bloc you need to update the senderId and username from sharedpreference
//in the ChatBloc constructor.

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainBlocProvider(
        child: MainPage(),
      ),
    );
  }
}
