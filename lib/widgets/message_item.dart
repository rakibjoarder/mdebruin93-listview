import 'package:flutter/material.dart';

//message Ui screen

class Message extends StatelessWidget {
  final String msg;
  // if senderId we get from api matched with our saved sender id it will
  //show the message from right side otherwise from left side
  final String direction;
  final String dateTime;
  final String senderName;

  Message({this.msg, this.direction, this.dateTime, this.senderName});

  @override
  Widget build(BuildContext context) {
    Widget avatar = CircleAvatar(
      backgroundColor: Colors.red,
      radius: 28,
      child: Text(senderName.substring(0, 1).toUpperCase()),
    );

    return new Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: ListTile(
        leading: direction == 'left' ? avatar : null,
        trailing: direction == 'right' ? avatar : null,
        title: Container(
          margin: EdgeInsets.only(
              right: direction == 'right' ? 0 : 40,
              left: direction == 'right' ? 40 : 0),
          decoration: new BoxDecoration(
            color: Colors.grey.shade300,
            border: new Border.all(
                color: Colors.grey.shade400,
                width: .25,
                style: BorderStyle.solid),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(direction == 'right' ? 0 : 10.0),
              topLeft: Radius.circular(direction == 'right' ? 10 : 0.0),
              bottomRight: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0),
            ),
          ),
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              userName(),
              userMessage(),
            ],
          ),
        ),
      ),
    );
  }

  Text userMessage() {
    return Text(
      msg,
      style: TextStyle(
        fontSize: 20.0,
        color: Color(0xff000000),
      ),
      textAlign: TextAlign.start,
    );
  }

  Text userName() {
    return Text(
      senderName,
      style: TextStyle(
        fontSize: 15.0,
        fontWeight: FontWeight.bold,
        color: Colors.grey.shade800,
      ),
      textAlign: TextAlign.end,
    );
  }
}
