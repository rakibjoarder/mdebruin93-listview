//this model is for sending message post request

class ChatModel {
  String senderName;
  String message;
  String sessionId;
  String senderId;
  String dateTimeOffset;

  ChatModel({this.senderName, this.message, this.sessionId});

  ChatModel.fromJson(Map<String, dynamic> json) {
    senderName = json['user'];
    message = json['message'];
    sessionId = json['sessionId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user'] = this.senderName;
    data['message'] = this.message;
    data['sessionId'] = this.sessionId;
    return data;
  }
}
