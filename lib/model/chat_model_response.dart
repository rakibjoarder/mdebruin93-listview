//this model is for recieved message response from the api
import 'dart:convert';

List<ModelResponse> responseFromJson(String str) => List<ModelResponse>.from(
    json.decode(str).map((x) => ModelResponse.fromJson(x)));

String responseFromToJson(List<ModelResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelResponse {
  String sessionId;
  String senderId;
  String senderName;
  String message;
  String dateTimeOffset;

  ModelResponse(
      {this.sessionId,
      this.senderId,
      this.senderName,
      this.message,
      this.dateTimeOffset});

  ModelResponse.fromJson(Map<String, dynamic> json) {
    sessionId = json['sessionId'];
    senderId = json['senderId'];
    senderName = json['senderName'];
    message = json['message'];
    dateTimeOffset = json['dateTimeOffset'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sessionId'] = this.sessionId;
    data['senderId'] = this.senderId;
    data['senderName'] = this.senderName;
    data['message'] = this.message;
    data['dateTimeOffset'] = this.dateTimeOffset;
    return data;
  }
}
