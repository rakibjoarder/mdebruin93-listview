import 'dart:core';
import 'package:flutter_app/bloc/blocprovider/chat_bloc_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/chat_model_response.dart';
import 'package:flutter_app/widgets/message_item.dart';
import 'package:flutter_app/widgets/show_toast.dart';

class ChatScreen extends StatefulWidget {
  @override
  _MyChatState createState() => new _MyChatState();
}

class _MyChatState extends State<ChatScreen> {
  ChatBloc bloc;

  @override
  void didChangeDependencies() {
    bloc = ChatBlocProvider.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: const Text(
            'Chat Page',
            textAlign: TextAlign.center,
          ),
        ),
        body: new Container(
            padding: EdgeInsets.only(bottom: 10),
            width: double.infinity,
            height: double.infinity,
            color: Colors.white,
            child: new Container(
              child: new Column(
                children: <Widget>[
                  //Chat list
                  new Flexible(
                    child: StreamBuilder<List<ModelResponse>>(
                        stream: bloc.messageStream,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? ListView.builder(
                                  padding: new EdgeInsets.all(8.0),
                                  reverse: true,
                                  controller: bloc.scrollController,
                                  itemBuilder: (_, int index) => Message(
                                    senderName: snapshot.data[index].senderName,
                                    direction: bloc.senderID ==
                                            snapshot.data[index].senderId
                                        ? 'right'
                                        : 'left',
                                    msg: snapshot.data[index].message,
                                    dateTime:
                                        snapshot.data[index].dateTimeOffset,
                                  ),
                                  itemCount: snapshot.data.length,
                                )
                              : Container();
                        }),
                  ),
                  Divider(height: 1.0),
                  Container(
                      decoration:
                          new BoxDecoration(color: Theme.of(context).cardColor),
                      child: Container(
                          margin: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: new Row(children: <Widget>[
                            //Enter Text message here
                            Flexible(
                              child: TextField(
                                controller: bloc.textController,
                                decoration: new InputDecoration.collapsed(
                                    hintText: "Enter message"),
                              ),
                            ),
                            //right send button
                            IconButton(
                              icon: Icon(Icons.send),
                              onPressed: () {
                                _sendMsg();
                              },
                            ),
                          ]))),
                ],
              ),
            )));
  }

  void _sendMsg() async {
    var response = await bloc.postMessage();
    if (!response) {
      showToast();
    }
  }
}
