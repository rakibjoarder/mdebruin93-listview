import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/blocprovider/chat_bloc_provider.dart';
import 'package:flutter_app/bloc/blocprovider/main_bloc_provider.dart';
import 'package:flutter_app/screen/chat_screen.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  MainBloc bloc;

  @override
  void didChangeDependencies() {
    bloc = MainBlocProvider.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.message),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ChatBlocProvider(child: ChatScreen())));
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          StreamBuilder(
            stream: bloc.listStream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return LinearProgressIndicator();
              }

              return Expanded(
                child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, ind) {
                      var formkey = GlobalKey<FormState>();
                      return Card(
                        elevation: 3,
                        margin: EdgeInsets.all(5),
                        child: ExpansionTile(
                          title: Text(snapshot.data[ind].title),
                          trailing: Text(
                            'Edit',
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          children: <Widget>[
                            Form(
                              key: formkey,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 15.0),
                                    child: TextFormField(
                                      initialValue: snapshot.data[ind].title,
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Required';
                                        } else {
                                          return null;
                                        }
                                      },
                                      onSaved: (value) {
                                        //closing keyboard

                                        bloc.edit(ind, value);
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        acceptButton(ind),
                                        rejectButton(ind),
                                        editButton(formkey),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    }),
              );
            },
          ),
        ],
      ),
    );
  }

  RaisedButton acceptButton(int ind) {
    return RaisedButton(
      color: Colors.green,
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Text(
        'Accept',
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16),
      ),
      onPressed: () {
        bloc.accept(ind);
      },
    );
  }

  RaisedButton rejectButton(int ind) {
    return RaisedButton(
      color: Colors.red,
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Text(
        'Reject',
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16),
      ),
      onPressed: () {
        bloc.reject(ind);
      },
    );
  }

  RaisedButton editButton(GlobalKey<FormState> formkey) {
    return RaisedButton(
      color: Colors.deepPurple,
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: Text(
        'Edit',
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16),
      ),
      onPressed: () {
        if (formkey.currentState.validate()) {
          formkey.currentState.save();
        }
      },
    );
  }
}
