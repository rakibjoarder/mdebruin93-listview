import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:flutter_app/model/item_model.dart';

abstract class BaseClass {
  dispose();
}

class MainBloc extends BaseClass {
  List<ItemModel> list = new List<ItemModel>();

  final listItemController = BehaviorSubject<List<ItemModel>>();

  Stream<List<ItemModel>> get listStream => listItemController.stream;
  //input
  StreamSink<List<ItemModel>> get listSink => listItemController.sink;

  MainBloc() {
    list.add(ItemModel(title: 'Hi this is Rakib data 1'));
    list.add(ItemModel(title: 'Hi this is dummy data 2'));
    list.add(ItemModel(title: 'Hi this is dummy data 3'));
    list.add(ItemModel(title: 'Hi this is Rakib data 4'));
    list.add(ItemModel(title: 'Hi this is dummy data 5'));
    list.add(ItemModel(title: 'Hi this is dummy data 6'));
    list.add(ItemModel(title: 'Hi this is dummy data 7'));
    list.add(ItemModel(title: 'Hi this is Rakib data 8'));
    list.add(ItemModel(title: 'Hi this is dummy data 9'));
    list.add(ItemModel(title: 'Hi this is dummy data 10'));
    list.add(ItemModel(title: 'Hi this is dummy data 11'));
    list.add(ItemModel(title: 'Hi this is dummy data 12'));
    list.add(ItemModel(title: 'Hi this is dummy data 13'));
    list.add(ItemModel(title: 'Hi this is dummy data 14'));
    list.add(ItemModel(title: 'Hi this is Rakib data 15'));

    //this delay is for showing loader ,basically when you fetch data from network it takes
    //some time. i have showed a loader on that delay. ignore the Future delay on your code
    // thats just for example.
    Future.delayed(Duration(seconds: 1), () {
      listSink.add(list);
    });
  }

  reject(int ind) {
    //perform reject here
    print(ind);
  }

  accept(int ind) {
    //perform aceppt here
    print(ind);
  }

  edit(int ind, String value) {
    //perform edit here
    print("Index : " + ind.toString() + ' ,Value : ' + value);
    list[ind].title = value;
    listSink.add(list);
  }

  @override
  dispose() {
    listItemController.close();
  }
}
