import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/chat_bloc.dart';
export 'package:flutter_app/bloc/chat_bloc.dart';

class ChatBlocProvider extends InheritedWidget {
  final ChatBloc bloc = new ChatBloc();
  final child;
  final Key key;

  ChatBlocProvider({this.child, this.key}) : super(child: child, key: key);

  static ChatBloc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(ChatBlocProvider)
              as ChatBlocProvider)
          .bloc;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
