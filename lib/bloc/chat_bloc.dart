import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_app/model/chat_model.dart';
import 'package:flutter_app/model/chat_model_response.dart';
import 'package:http/http.dart' as http;

abstract class BaseClass {
  dispose();
}

class ChatBloc extends BaseClass {
  String senderID;
  String senderName, sessionId;
  //streamController
  final listItemController = BehaviorSubject<List<ModelResponse>>();

  //it will stream all the message responses in the ui screen that we have
  //fetch from the api
  Stream<List<ModelResponse>> get messageStream => listItemController.stream;

  //we will fetch all the messages from api and will sink into it
  StreamSink<List<ModelResponse>> get msgSink => listItemController.sink;

  final TextEditingController textController = new TextEditingController();
  String get getMsg => textController.text;

  //scrollController help us to scrool down the screen everytime a new
  //message is post
  ScrollController scrollController = new ScrollController();

  ChatBloc() {
    //TODO change here
    //change this sender Id from sharedPreference
    senderID = '4d22fe59-480a-433f-8632-f72a38865e41';
    sessionId = '3fa85f64-5717-4562-b3fc-2c963f66afa6';
    //change this senderName  from sharedPreference
    senderName = 'Rakib Joarder';

    getMessage();
  }

  //get all the message
  getMessage() async {
    var errorFlag = false;
    var url =
        'http://confapifiverr.azurewebsites.net/conf/GetMessages?sessionId=$sessionId';
    var response = await http.get(url).catchError((onError) {
      print(onError);
      errorFlag = true;
    });
    if (!errorFlag) {
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 200) {
        List<ModelResponse> list = responseFromJson(response.body);
        msgSink.add(list.reversed.toList());
      }
    }
  }

  //post message from here
  postMessage() async {
    var errorFlag = false;
    // if message is null or '' it will show a toast
    if (getMsg == null || getMsg == '') {
      return false;
    } else {
      var chatItem = ChatModel(
          senderName: senderName, message: getMsg, sessionId: sessionId);

      var url = 'http://confapifiverr.azurewebsites.net/conf/PostMessage';

      var response = await http.post(url,
          body: jsonEncode(chatItem.toJson()),
          headers: {'content-type': 'application/json'}).catchError((onError) {
        print(onError);
        errorFlag = true;
      });
      if (!errorFlag) {
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');
        senderID = jsonDecode(response.body)['senderId'];
        textController.clear();
        scrollController.animateTo(
          0.0,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
        getMessage();
      }
    }

    return true;
  }

  @override
  dispose() {
    listItemController.close();
    textController.dispose();
    scrollController.dispose();
  }
}
